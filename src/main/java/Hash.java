import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Date;

public class Hash
{
    /**
     * Applies Sha256 to a string and returns the result.
     *
     * @param input The data to be inputted.
     * @return A hashed {@link String} representing the data as SHA-256.
     */
    public static String applySHA256(String input)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++)
            {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args)
    {
        String hash;
        BigInteger target = new BigInteger("000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 16);

        System.out.println("Mining with target:\t" + target);

        for (int nounce = 0; nounce < Integer.MAX_VALUE; nounce++)
        {
            String hashContent = String.format("Hello world! %s %s", nounce, new Date().getTime());
            hash = applySHA256(hashContent);

            BigInteger value = new BigInteger(hash, 16);

            if (value.compareTo(target) < 0)
            {
                System.out.println(hash + " meets the target");
                System.out.println(hashContent);
                break;
            }
        }
    }
}
